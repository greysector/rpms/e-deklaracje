Summary: Submit tax forms for natural persons electronically
Summary(pl): Składanie elektronicznych formularzy podatkowych dla osób fizycznych
Name: e-deklaracje
License: Redistributable
Version: 13.0.0
Release: 1
URL: https://www.podatki.gov.pl/e-deklaracje/aplikacja-e-deklaracje-desktop/
Source0: https://www.podatki.gov.pl/media/3103/e-deklaracjedesktop.air
Source1: https://www.podatki.gov.pl/media/3034/e-deklaracjedesktop.md5
Source2: https://www.podatki.gov.pl/media/3032/instrukcjadesktop_v13-0-0_b.pdf
Source3: https://www.podatki.gov.pl/media/3192/instrukcjelinux.pdf
Source10: %{name}.desktop
Source11: %{name}.sh
BuildArch: noarch
BuildRequires: desktop-file-utils
BuildRequires: sed
BuildRequires: unzip
Requires: acroread >= 9.0.0
Requires: adobe-air
Requires: hicolor-icon-theme

%description
e-Deklaracje Desktop application is a tool for natural persons to submit tax
forms signed with "authorization data" (digital signature ensuring form and
request authenticity based on authorization data) electronically.

%description -l pl
Aplikacja e-Deklaracje Desktop to narzędzie, które umożliwia tylko osobom
fizycznym (podatnikom, płatnikom) wysyłanie drogą elektroniczną deklaracji
podpisanych "danymi autoryzującymi" (podpisem elektronicznym zapewniającym
autentyczność deklaracji i podań opartym na danych autoryzujących).

%prep
pushd %{_sourcedir}
sed -e 's,DeklaracjeD,deklaracjed,' -e 's,\r,,' %{S:1} > MD5SUM
md5sum -c MD5SUM
rm MD5SUM
popd
%setup -qc
cp -p %{S:2} %{S:3} .

%build

%install
install -Dpm755 %{S:11} %{buildroot}%{_bindir}/%{name}
install -dm755 %{buildroot}%{_datadir}/%{name}
unzip -qq %{S:0} -d %{buildroot}%{_datadir}/%{name}
for s in 16 32 48 128 ; do
  install -Dpm644 assets/icons/icon${s}.png \
     %{buildroot}%{_datadir}/icons/hicolor/${s}x${s}/apps/e-deklaracje.png
done

desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:10}

%files
%lang(pl) %doc instrukcjadesktop_v13-0-0_b.pdf instrukcjelinux.pdf
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/e-deklaracje.png

%changelog
* Thu Mar 04 2021 Dominik Mierzejewski <rpm@greysector.net> 13.0.0-1
- update to 13.0.0

* Thu Dec 31 2020 Dominik Mierzejewski <rpm@greysector.net> 12.0.5-1
- update to 12.0.5

* Fri Aug 21 2020 Dominik Mierzejewski <rpm@greysector.net> 12.0.4-1
- update to 12.0.4 (adds support for VAT-10 and VAT-11 forms)

* Tue Apr 14 2020 Dominik Mierzejewski <rpm@greysector.net> 12.0.3-1
- initial build
